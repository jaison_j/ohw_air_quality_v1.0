import paho.mqtt.client as mqtt
from influxdb import InfluxDBClient
import json
import dateutil.parser
import time
broker_address = "mqtt.*****.org"
database="ohwairquality"
measurement="AQ_dev1"
username = "username"
password = "12345678"
def on_connect(client, userdata, flags, rc):
  print("Connected with result code {0}".format(str(rc)))  # Print result of connection attempt
  client.subscribe("AQ_WTout/#")

def on_message(client, userdata, msg):
  try:
    output=json.loads(msg.payload.decode())
    print(output)
    json_body = [
    {
  
    "measurement": msg.topic,
    "tags":{},
    "fields": output
    }]
    print(json_body)
    influxdbClient.write_points(json_body)
  except:
    pass

client = mqtt.Client("anyname")
client.username_pw_set(username, password)
influxdbClient = InfluxDBClient(host='192.168.*.**', port=8086,username='admin', password='12345678',database=database)
print(influxdbClient)
client.on_connect = on_connect
client.on_message = on_message
client.connect(broker_address, 1883,60)
client.loop_forever()  # Start networking daemon
